<?php

namespace App\Http\Requests\Api\Auth\Register;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'min:5', 'max:15'],
            'password' => ['required', 'min:8', 'max:20'],
            'email' => ['required', 'unique:users', 'email address']
        ];
    }
}
