<?php

declare(strict_types=1);

namespace App\Traits\Tests;

use App\Models\User;

trait AuthUser
{
    private User $user;
    private string $token;
    private array $request = ['name' => 'name', 'password' => 'password', 'email' => 'email@mail.ru'];

    public function setup(): void
    {
        parent::setUp();

        $this->user = User::create($this->request);
        $this->token = $this->refreshToken();
    }

    public function refreshToken(): string|bool
    {
        return auth()->attempt($this->request);
    }

    public function editRoleUserOnManager(): void
    {
        $this->user->role = 'manager';
        $this->user->save();
        $this->refreshToken();
    }
}
