<?php

use App\Http\Controllers\Api\FormController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\AuthController;

Route::middleware('auth:api')->group(function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', [AuthController::class, 'login']);
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('refresh', [AuthController::class, 'refresh']);
        Route::post('register', [AuthController::class, 'register']);
    });

    Route::apiResource('forms', FormController::class)->only(['index', 'store', 'show']);

    Route::middleware('user.role.manager')->group(function () {
        Route::apiResource('forms', FormController::class)->only('update');
        Route::get('/forms_for_manager', [FormController::class, 'allForms']);
    });
});
